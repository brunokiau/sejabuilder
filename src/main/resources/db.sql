CREATE TABLE IF NOT EXISTS `customer` (
  `id` varchar(32) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `birth_date` date,
  PRIMARY KEY (`id`)
);
