package com.bruno.sejabuilder.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bruno.sejabuilder.domain.Customer;

/**
 * Repositório de clientes.
 * @author brunoaraujo
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

}
