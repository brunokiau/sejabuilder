package com.bruno.sejabuilder.service;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bruno.sejabuilder.domain.Customer;
import com.bruno.sejabuilder.repository.CustomerRepository;

/**
 * Serviços da entidade {@link Customer}.
 * @author brunoaraujo
 */
@Service
public class CustomerService {
	private CustomerRepository repository;

	@Autowired
	public CustomerService(CustomerRepository repository) {
		this.repository = repository;
	}

	/**
	 * Busca por clientes.
	 * @param cpf CPf (Opcional)
	 * @param name Nome (Opcional)
	 * @param page Paginação.
	 * @return Clientes encontrados.
	 */
	public Page<Customer> search(String cpf, String name, Pageable page) {

		if (!StringUtils.hasText(cpf) && !StringUtils.hasText(name)) {
			return repository.findAll(page);
		}

		final ExampleMatcher exampleMatcher = ExampleMatcher.matchingAll()
				.withMatcher("cpf", ExampleMatcher.GenericPropertyMatchers.startsWith())
				.withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase());

		final Customer customer = new Customer(null, name, cpf, null);
		final Example<Customer> example = Example.of(customer, exampleMatcher);

		return this.repository.findAll(example, page);
	}

	/**
	 * Obtém um cliente específico.
	 * @param id ID do cliente.
	 * @return Cliente.
	 */
	public Optional<Customer> get(String id) {
		return this.repository.findById(id);
	}

	/**
	 * Remove um cliente.
	 * @param id ID do cliente.
	 */
	public void remove(String id) {
		this.repository.deleteById(id);
	}

	/**
	 * Salva um cliente.<BR>
	 * Pode criar um novo cliente ou alterar um já existente.
	 * @param customer DAdos do cliente.
	 * @return Dados do cliente atualizados.
	 */
	public Customer save(Customer customer) {
		Customer toSave = customer;

		if (customer.getId() == null) {
			final String id = UUID.randomUUID().toString();
			toSave = new Customer(id, customer.getName(), customer.getCpf(), customer.getBirthDate());
		} else {
			// Validar ID
			UUID.fromString(customer.getId());
		}

		return this.repository.saveAndFlush(toSave);
	}

}
