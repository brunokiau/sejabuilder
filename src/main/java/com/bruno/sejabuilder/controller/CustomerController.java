package com.bruno.sejabuilder.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bruno.sejabuilder.PatchHelper;
import com.bruno.sejabuilder.domain.Customer;
import com.bruno.sejabuilder.service.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

@RestController
@RequestMapping("/api/clientes")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@GetMapping()
	public ResponseEntity<Page<Customer>> search(@RequestParam(required = false) String cpf,
			@RequestParam(required = false) String name,
			@PageableDefault(page = 0, size = 5, sort = "name", direction = Direction.ASC) Pageable page) {
		final Page<Customer> result = this.customerService.search(cpf, name, page);
		return ResponseEntity.ok(result);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<Customer> get(@PathVariable("id") String id) {
		final Optional<Customer> customer = this.customerService.get(id);

		if (customer.isPresent()) {
			return ResponseEntity.ok(customer.get());
		}

		return ResponseEntity.notFound().build();
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<Void> put(@PathVariable("id") String id, @Valid @RequestBody Customer customer) {
		final Customer toSave = new Customer(id, customer.getName(), customer.getCpf(), customer.getBirthDate());
		this.customerService.save(toSave);
		return ResponseEntity.noContent().build();
	}

	@PostMapping
	public ResponseEntity<Customer> post(@Valid @RequestBody Customer customer) {
		final Customer saved = this.customerService.save(customer);
		final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(saved.getId()).toUri();
		return ResponseEntity.created(location).body(saved);
	}

	@PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
	public ResponseEntity<Customer> patch(@PathVariable("id") String id, @RequestBody JsonPatch patch) {
		final Optional<Customer> current = this.customerService.get(id);

		if (current.isPresent()) {
			final Customer customer = current.get();

			try {
				Customer patchedCustomer = PatchHelper.applyPatch(customer, Customer.class, patch);
				Customer savedCustomer = this.customerService.save(patchedCustomer);

				return ResponseEntity.ok(savedCustomer);
			} catch (JsonPatchException | JsonProcessingException e) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		}

		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).allow(HttpMethod.PUT).build();
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Void> remove(@PathVariable("id") String id) {
		this.customerService.remove(id);
		return ResponseEntity.noContent().build();
	}

}
