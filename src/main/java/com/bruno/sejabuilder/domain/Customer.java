package com.bruno.sejabuilder.domain;

import java.time.LocalDate;
import java.time.Period;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Customer {

	@Id
	private String id;
	@NotBlank(message = "Nome é obrigatório")
	private String name;
	@CPF(message = "CPF inválido")
	@NotBlank(message = "CPF é obrigatório")
	private String cpf;
	@NotNull(message = "Data de nascimento é obrigatória")
	private LocalDate birthDate;
	
	public Customer() {
		
	}

	public Customer(String id, String name, String cpf, LocalDate birthDate) {
		super();
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.birthDate = birthDate;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return this.cpf;
	}

	/**
	 * @return the birthDate
	 */
	public LocalDate getBirthDate() {
		return this.birthDate;
	}

	@JsonProperty()
	public Integer getAge() {
		if (this.birthDate != null) {
			return Period.between(this.birthDate, LocalDate.now()).getYears();
		}

		return null;
	}

}
