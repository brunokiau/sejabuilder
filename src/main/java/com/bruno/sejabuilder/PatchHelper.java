package com.bruno.sejabuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

/**
 * Helper para aplicação de patches.
 * @author brunoaraujo
 */
public class PatchHelper {
	/**
	 * Aplica as operações de patch no objeto.
	 * @param <T> Tipo do objeto.
	 * @param object Instância do Objeto.
	 * @param objectClass Classe do objeto.
	 * @param patch Patch a ser aplicado.
	 * @return Objeto com patch aplicado.
	 * @throws JsonPatchException  Em caso de erro ao aplicar o patch.
	 * @throws JsonProcessingException  Em caso de erro ao aplicar o patch. 
	 */
	public static <T> T applyPatch(T object, Class<T> objectClass, JsonPatch patch) throws JsonPatchException, JsonProcessingException {
		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);

		final JsonNode patched = patch.apply(objectMapper.convertValue(object, JsonNode.class));
		final T patchedObject = objectMapper.treeToValue(patched, objectClass);
		
		return patchedObject;
	}
}
