package com.bruno.sejabuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnOk() throws Exception {
		this.mockMvc.perform(get("/api/clientes")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void basicCrudSequence() throws Exception {
		final String id = UUID.randomUUID().toString();

		final String json = "{\"name\": \"Teste ABC\",\"cpf\": \"76199468023\",\"birthDate\": \"1980-01-01\"}";

		this.mockMvc.perform(get("/api/clientes/{id}", id)).andDo(print()).andExpect(status().isNotFound());
		this.mockMvc.perform(put("/api/clientes/{id}", id).contentType(MediaType.APPLICATION_JSON).content(json)).andDo(print()).andExpect(status().isNoContent());
		this.mockMvc.perform(get("/api/clientes/{id}", id)).andDo(print()).andExpect(status().isOk());
		this.mockMvc.perform(delete("/api/clientes/{id}", id)).andDo(print()).andExpect(status().isNoContent());
		this.mockMvc.perform(get("/api/clientes/{id}", id)).andDo(print()).andExpect(status().isNotFound());
	}
}